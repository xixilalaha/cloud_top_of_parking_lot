
layui.define(['laypage', 'form'], function (exports) {
    "use strict";

    var IconPicker =function () {
        this.v = '0.1.beta';
    }, _MOD = 'iconPicker',
        _this = this,
        $ = layui.jquery,
        laypage = layui.laypage,
        form = layui.form,
        BODY = 'body',
        TIPS = '请选择图标';

    /**
     * 渲染组件
     */
    IconPicker.prototype.render = function(options){
        var opts = options,
            // DOM选择器
            elem = opts.elem,
            // 数据类型：fontClass/unicode
            type = opts.type == null ? 'fontClass' : opts.type,
            // 是否分页：true/false
            page = opts.page,
            // 每页显示数量
            limit = limit == null ? 12 : opts.limit,
            // 是否开启搜索：true/false
            search = opts.search == null ? true : opts.search,
            // 点击回调
            click = opts.click,
            // 渲染成功后的回调
            success = opts.success,
            // json数据
            data = {},
            // 唯一标识
            tmp = new Date().getTime(),
            // 是否使用的class数据
            isFontClass = opts.type === 'fontClass',
            // 初始化时input的值
            ORIGINAL_ELEM_VALUE = $(elem).val(),
            TITLE = 'layui-select-title',
            TITLE_ID = 'layui-select-title-' + tmp,
            ICON_BODY = 'layui-iconpicker-' + tmp,
            PICKER_BODY = 'layui-iconpicker-body-' + tmp,
            PAGE_ID = 'layui-iconpicker-page-' + tmp,
            LIST_BOX = 'layui-iconpicker-list-box',
            selected = 'layui-form-selected',
            unselect = 'layui-unselect';

        var a = {
            init: function () {
                data = common.getData[type]();

                a.hideElem().createSelect().createBody().toggleSelect();
                a.preventEvent().inputListen();
                common.loadCss();

                if (success) {
                    success(this.successHandle());
                }

                return a;
            },
            successHandle: function(){
                var d = {
                    options: opts,
                    data: data,
                    id: tmp,
                    elem: $('#' + ICON_BODY)
                };
                return d;
            },
            /**
             * 隐藏elem
             */
            hideElem: function () {
                $(elem).hide();
                return a;
            },
            /**
             * 绘制select下拉选择框
             */
            createSelect: function () {
                var oriIcon = '<i class="layui-icon">';

                // 默认图标
                if(ORIGINAL_ELEM_VALUE === '') {
                    if(isFontClass) {
                        ORIGINAL_ELEM_VALUE = 'layui-icon-circle-dot';
                    } else {
                        ORIGINAL_ELEM_VALUE = '&#xe617;';
                    }
                }

                if (isFontClass) {
                    oriIcon = '<i class="layui-icon '+ ORIGINAL_ELEM_VALUE +'">';
                } else {
                    oriIcon += ORIGINAL_ELEM_VALUE;
                }
                oriIcon += '</i>';

                var selectHtml = '<div class="layui-iconpicker layui-unselect layui-form-select" id="'+ ICON_BODY +'">' +
                    '<div class="'+ TITLE +'" id="'+ TITLE_ID +'">' +
                        '<div class="layui-iconpicker-item">'+
                            '<span class="layui-iconpicker-icon layui-unselect">' +
                                oriIcon +
                            '</span>'+
                            '<i class="layui-edge"></i>' +
                        '</div>'+
                    '</div>' +
                    '<div class="layui-anim layui-anim-upbit" style="">' +
                        '123' +
                    '</div>';
                $(elem).after(selectHtml);
                return a;
            },
            /**
             * 展开/折叠下拉框
             */
            toggleSelect: function () {
                var item = '#' + TITLE_ID + ' .layui-iconpicker-item,#' + TITLE_ID + ' .layui-iconpicker-item .layui-edge';
                a.event('click', item, function (e) {
                    var $icon = $('#' + ICON_BODY);
                    if ($icon.hasClass(selected)) {
                        $icon.removeClass(selected).addClass(unselect);
                    } else {
                        // 隐藏其他picker
                        $('.layui-form-select').removeClass(selected);
                        // 显示当前picker
                        $icon.addClass(selected).removeClass(unselect);
                    }
                    e.stopPropagation();
                });
                return a;
            },
            /**
             * 绘制主体部分
             */
            createBody: function () {
                // 获取数据
                var searchHtml = '';

                if (search) {
                    searchHtml = '<div class="layui-iconpicker-search">' +
                        '<input class="layui-input">' +
                        '<i class="layui-icon">&#xe615;</i>' +
                        '</div>';
                }

                // 组合dom
                var bodyHtml = '<div class="layui-iconpicker-body" id="'+ PICKER_BODY +'">' +
                    searchHtml +
                        '<div class="'+ LIST_BOX +'"></div> '+
                     '</div>';
                $('#' + ICON_BODY).find('.layui-anim').eq(0).html(bodyHtml);
                a.search().createList().check().page();

                return a;
            },
            /**
             * 绘制图标列表
             * @param text 模糊查询关键字
             * @returns {string}
             */
            createList: function (text) {
                var d = data,
                    l = d.length,
                    pageHtml = '',
                    listHtml = $('<div class="layui-iconpicker-list">')//'<div class="layui-iconpicker-list">';

                // 计算分页数据
                var _limit = limit, // 每页显示数量
                    _pages = l % _limit === 0 ? l / _limit : parseInt(l / _limit + 1), // 总计多少页
                    _id = PAGE_ID;

                // 图标列表
                var icons = [];

                for (var i = 0; i < l; i++) {
                    var obj = d[i];

                    // 判断是否模糊查询
                    if (text && obj.indexOf(text) === -1) {
                        continue;
                    }

                    // 每个图标dom
                    var icon = '<div class="layui-iconpicker-icon-item" title="'+ obj +'">';
                    if (isFontClass){
                        icon += '<i class="layui-icon '+ obj +'"></i>';
                    } else {
                        icon += '<i class="layui-icon">'+ obj.replace('amp;', '') +'</i>';
                    }
                    icon += '</div>';

                    icons.push(icon);
                }

                // 查询出图标后再分页
                l = icons.length;
                _pages = l % _limit === 0 ? l / _limit : parseInt(l / _limit + 1);
                for (var i = 0; i < _pages; i++) {
                    // 按limit分块
                    var lm = $('<div class="layui-iconpicker-icon-limit" id="layui-iconpicker-icon-limit-' + tmp + (i+1) +'">');

                    for (var j = i * _limit; j < (i+1) * _limit && j < l; j++) {
                        lm.append(icons[j]);
                    }

                    listHtml.append(lm);
                }

                // 无数据
                if (l === 0) {
                    listHtml.append('<p class="layui-iconpicker-tips">无数据</p>');
                }

                // 判断是否分页
                if (page){
                    $('#' + PICKER_BODY).addClass('layui-iconpicker-body-page');
                    pageHtml = '<div class="layui-iconpicker-page" id="'+ PAGE_ID +'">' +
                        '<div class="layui-iconpicker-page-count">' +
                        '<span id="'+ PAGE_ID +'-current">1</span>/' +
                        '<span id="'+ PAGE_ID +'-pages">'+ _pages +'</span>' +
                        ' (<span id="'+ PAGE_ID +'-length">'+ l +'</span>)' +
                        '</div>' +
                        '<div class="layui-iconpicker-page-operate">' +
                        '<i class="layui-icon" id="'+ PAGE_ID +'-prev" data-index="0" prev>&#xe603;</i> ' +
                        '<i class="layui-icon" id="'+ PAGE_ID +'-next" data-index="2" next>&#xe602;</i> ' +
                        '</div>' +
                        '</div>';
                }


                $('#' + ICON_BODY).find('.layui-anim').find('.' + LIST_BOX).html('').append(listHtml).append(pageHtml);
                return a;
            },
            // 阻止Layui的一些默认事件
            preventEvent: function() {
                var item = '#' + ICON_BODY + ' .layui-anim';
                a.event('click', item, function (e) {
                    e.stopPropagation();
                });
                return a;
            },
            // 分页
            page: function () {
                var icon = '#' + PAGE_ID + ' .layui-iconpicker-page-operate .layui-icon';

                $(icon).unbind('click');
                a.event('click', icon, function (e) {
                   var elem = e.currentTarget,
                       total = parseInt($('#' +PAGE_ID + '-pages').html()),
                       isPrev = $(elem).attr('prev') !== undefined,
                       // 按钮上标的页码
                       index = parseInt($(elem).attr('data-index')),
                       $cur = $('#' +PAGE_ID + '-current'),
                       // 点击时正在显示的页码
                       current = parseInt($cur.html());

                    // 分页数据
                    if (isPrev && current > 1) {
                        current=current-1;
                        $(icon + '[prev]').attr('data-index', current);
                    } else if (!isPrev && current < total){
                        current=current+1;
                        $(icon + '[next]').attr('data-index', current);
                    }
                    $cur.html(current);

                    // 图标数据
                    $('#'+ ICON_BODY + ' .layui-iconpicker-icon-limit').hide();
                    $('#layui-iconpicker-icon-limit-' + tmp + current).show();
                    e.stopPropagation();
                });
                return a;
            },
            /**
             * 搜索
             */
            search: function () {
                var item = '#' + PICKER_BODY + ' .layui-iconpicker-search .layui-input';
                a.event('input propertychange', item, function (e) {
                    var elem = e.target,
                        t = $(elem).val();
                    a.createList(t);
                });
                return a;
            },
            /**
             * 点击选中图标
             */
            check: function () {
                var item = '#' + PICKER_BODY + ' .layui-iconpicker-icon-item';
                a.event('click', item, function (e) {
                    var el = $(e.currentTarget).find('.layui-icon'),
                        icon = '';
                    if (isFontClass) {
                        var clsArr = el.attr('class').split(/[\s\n]/),
                            cls = clsArr[1],
                            icon = cls;
                        $('#' + TITLE_ID).find('.layui-iconpicker-item .layui-icon').html('').attr('class', clsArr.join(' '));
                    } else {
                        var cls = el.html(),
                            icon = cls;
                        $('#' + TITLE_ID).find('.layui-iconpicker-item .layui-icon').html(icon);
                    }

                    $('#' + ICON_BODY).removeClass(selected).addClass(unselect);
                    $(elem).val(icon).attr('value', icon);
                    // 回调
                    if (click) {
                        click({
                            icon: icon
                        });
                    }

                });
                return a;
            },
            // 监听原始input数值改变
            inputListen: function(){
                var el = $(elem);
                // TODO
                a.event('change', elem, function(){
                    var value = el.val();
                    console.log(value)
                })
                // el.change(function(){

                // });
                return a;
            },
            event: function (evt, el, fn) {
                $(BODY).on(evt, el, fn);
            }
        };

        var common = {
            /**
             * 加载样式表
             */
            loadCss: function () {
                var css = '.layui-iconpicker {max-width: 280px;}.layui-iconpicker .layui-anim{display:none;position:absolute;left:0;top:42px;padding:5px 0;z-index:899;min-width:100%;border:1px solid #d2d2d2;max-height:300px;overflow-y:auto;background-color:#fff;border-radius:2px;box-shadow:0 2px 4px rgba(0,0,0,.12);box-sizing:border-box;}.layui-iconpicker-item{border:1px solid #e6e6e6;width:90px;height:38px;border-radius:4px;cursor:pointer;position:relative;}.layui-iconpicker-icon{border-right:1px solid #e6e6e6;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;width:60px;height:100%;float:left;text-align:center;background:#fff;transition:all .3s;}.layui-iconpicker-icon i{line-height:38px;font-size:18px;}.layui-iconpicker-item > .layui-edge{left:70px;}.layui-iconpicker-item:hover{border-color:#D2D2D2!important;}.layui-iconpicker-item:hover .layui-iconpicker-icon{border-color:#D2D2D2!important;}.layui-iconpicker.layui-form-selected .layui-anim{display:block;}.layui-iconpicker-body{padding:6px;}.layui-iconpicker .layui-iconpicker-list{background-color:#fff;border:1px solid #ccc;border-radius:4px;}.layui-iconpicker .layui-iconpicker-icon-item{display:inline-block;width:21.1%;line-height:36px;text-align:center;cursor:pointer;vertical-align:top;height:36px;margin:4px;border:1px solid #ddd;border-radius:2px;transition:300ms;}.layui-iconpicker .layui-iconpicker-icon-item i.layui-icon{font-size:17px;}.layui-iconpicker .layui-iconpicker-icon-item:hover{background-color:#eee;border-color:#ccc;-webkit-box-shadow:0 0 2px #aaa,0 0 2px #fff inset;-moz-box-shadow:0 0 2px #aaa,0 0 2px #fff inset;box-shadow:0 0 2px #aaa,0 0 2px #fff inset;text-shadow:0 0 1px #fff;}.layui-iconpicker-search{position:relative;margin:0 0 6px 0;border:1px solid #e6e6e6;border-radius:2px;transition:300ms;}.layui-iconpicker-search:hover{border-color:#D2D2D2!important;}.layui-iconpicker-search .layui-input{cursor:text;display:inline-block;width:86%;border:none;padding-right:0;margin-top:1px;}.layui-iconpicker-search .layui-icon{position:absolute;top:11px;right:4%;}.layui-iconpicker-tips{text-align:center;padding:8px 0;cursor:not-allowed;}.layui-iconpicker-page{margin-top:6px;margin-bottom:-6px;font-size:12px;padding:0 2px;}.layui-iconpicker-page-count{display:inline-block;}.layui-iconpicker-page-operate{display:inline-block;float:right;cursor:default;}.layui-iconpicker-page-operate .layui-icon{font-size:12px;cursor:pointer;}.layui-iconpicker-body-page .layui-iconpicker-icon-limit{display:none;}.layui-iconpicker-body-page .layui-iconpicker-icon-limit:first-child{display:block;}';
                var $style = $('head').find('style[iconpicker]');
                if ($style.length === 0) {
                    $('head').append('<style rel="stylesheet" iconpicker>'+css+'</style>');
                }
            },
            /**
             * 获取数据
             */
            getData: {
                fontClass: function () {
                    var arr = ["layui-icon-check-circle","layui-icon-CI","layui-icon-Dollar","layui-icon-compass","layui-icon-close-circle","layui-icon-frown","layui-icon-info-circle","layui-icon-left-circle","layui-icon-down-circle","layui-icon-EURO","layui-icon-copyright","layui-icon-minus-circle","layui-icon-meh","layui-icon-plus-circle","layui-icon-play-circle","layui-icon-question-circle","layui-icon-Pound","layui-icon-right-circle","layui-icon-smile","layui-icon-trademark","layui-icon-time-circle","layui-icon-timeout","layui-icon-earth","layui-icon-YUAN","layui-icon-up-circle","layui-icon-warning-circle","layui-icon-sync","layui-icon-transaction","layui-icon-undo","layui-icon-redo","layui-icon-reload","layui-icon-reloadtime","layui-icon-message","layui-icon-dashboard","layui-icon-issuesclose","layui-icon-poweroff","layui-icon-logout","layui-icon-login","layui-icon-piechart","layui-icon-setting","layui-icon-eye","layui-icon-location","layui-icon-edit-square","layui-icon-export","layui-icon-save","layui-icon-Import","layui-icon-appstore","layui-icon-close-square","layui-icon-down-square","layui-icon-layout","layui-icon-left-square","layui-icon-play-square","layui-icon-control","layui-icon-codelibrary","layui-icon-detail","layui-icon-minus-square","layui-icon-plus-square","layui-icon-right-square","layui-icon-project","layui-icon-wallet","layui-icon-up-square","layui-icon-calculator","layui-icon-interation","layui-icon-check-square","layui-icon-border","layui-icon-border-outer","layui-icon-border-top","layui-icon-border-bottom","layui-icon-border-left","layui-icon-border-right","layui-icon-border-inner","layui-icon-border-verticle","layui-icon-border-horizontal","layui-icon-radius-bottomleft","layui-icon-radius-bottomright","layui-icon-radius-upleft","layui-icon-radius-upright","layui-icon-radius-setting","layui-icon-adduser","layui-icon-deleteteam","layui-icon-deleteuser","layui-icon-addteam","layui-icon-user","layui-icon-team","layui-icon-areachart","layui-icon-linechart","layui-icon-barchart","layui-icon-pointmap","layui-icon-container","layui-icon-database","layui-icon-sever","layui-icon-mobile","layui-icon-tablet","layui-icon-redenvelope","layui-icon-book","layui-icon-filedone","layui-icon-reconciliation","layui-icon-file-exception","layui-icon-filesync","layui-icon-filesearch","layui-icon-solution","layui-icon-fileprotect","layui-icon-file-add","layui-icon-file-excel","layui-icon-file-exclamation","layui-icon-file-pdf","layui-icon-file-image","layui-icon-file-markdown","layui-icon-file-unknown","layui-icon-file-ppt","layui-icon-file-word","layui-icon-file","layui-icon-file-zip","layui-icon-file-text","layui-icon-file-copy","layui-icon-snippets","layui-icon-audit","layui-icon-diff","layui-icon-Batchfolding","layui-icon-securityscan","layui-icon-propertysafety","layui-icon-safetycertificate","layui-icon-insurance","layui-icon-alert","layui-icon-delete","layui-icon-hourglass","layui-icon-bulb","layui-icon-experiment","layui-icon-bell","layui-icon-trophy","layui-icon-rest","layui-icon-USB","layui-icon-skin","layui-icon-home","layui-icon-bank","layui-icon-filter","layui-icon-funnelplot","layui-icon-like","layui-icon-unlike","layui-icon-unlock","layui-icon-lock","layui-icon-customerservice","layui-icon-flag","layui-icon-moneycollect","layui-icon-medicinebox","layui-icon-shop","layui-icon-rocket","layui-icon-shopping","layui-icon-folder","layui-icon-folder-open","layui-icon-folder-add","layui-icon-deploymentunit","layui-icon-accountbook","layui-icon-contacts","layui-icon-carryout","layui-icon-calendar-check","layui-icon-calendar","layui-icon-scan","layui-icon-select","layui-icon-boxplot","layui-icon-build","layui-icon-sliders","layui-icon-laptop","layui-icon-barcode","layui-icon-camera","layui-icon-cluster","layui-icon-gateway","layui-icon-car","layui-icon-printer","layui-icon-read","layui-icon-cloud-server","layui-icon-cloud-upload","layui-icon-cloud","layui-icon-cloud-download","layui-icon-cloud-sync","layui-icon-video","layui-icon-notification","layui-icon-sound","layui-icon-radarchart","layui-icon-qrcode","layui-icon-fund","layui-icon-image","layui-icon-mail","layui-icon-table","layui-icon-idcard","layui-icon-creditcard","layui-icon-heart","layui-icon-block","layui-icon-error","layui-icon-star","layui-icon-gold","layui-icon-heatmap","layui-icon-wifi","layui-icon-attachment","layui-icon-edit","layui-icon-key","layui-icon-api","layui-icon-disconnect","layui-icon-highlight","layui-icon-monitor","layui-icon-link","layui-icon-man","layui-icon-percentage","layui-icon-search","layui-icon-pushpin","layui-icon-phone","layui-icon-shake","layui-icon-tag","layui-icon-wrench","layui-icon-woman","layui-icon-tags","layui-icon-scissor","layui-icon-mr","layui-icon-share","layui-icon-branches","layui-icon-fork","layui-icon-shrink","layui-icon-arrawsalt","layui-icon-verticalright","layui-icon-verticalleft","layui-icon-right","layui-icon-left","layui-icon-up","layui-icon-down","layui-icon-fullscreen","layui-icon-fullscreen-exit","layui-icon-doubleleft","layui-icon-doubleright","layui-icon-arrowright","layui-icon-arrowup","layui-icon-arrowleft","layui-icon-arrowdown","layui-icon-upload","layui-icon-colum-height","layui-icon-vertical-align-botto","layui-icon-vertical-align-middl","layui-icon-totop","layui-icon-vertical-align-top","layui-icon-download","layui-icon-sort-descending","layui-icon-sort-ascending","layui-icon-fall","layui-icon-swap","layui-icon-stock","layui-icon-rise","layui-icon-indent","layui-icon-outdent","layui-icon-menu","layui-icon-unorderedlist","layui-icon-orderedlist","layui-icon-align-right","layui-icon-align-center","layui-icon-align-left","layui-icon-pic-center","layui-icon-pic-right","layui-icon-pic-left","layui-icon-bold","layui-icon-font-colors","layui-icon-exclaimination","layui-icon-font-size","layui-icon-infomation","layui-icon-line-height","layui-icon-strikethrough","layui-icon-underline","layui-icon-number","layui-icon-italic","layui-icon-code","layui-icon-column-width","layui-icon-check","layui-icon-ellipsis","layui-icon-dash","layui-icon-close","layui-icon-enter","layui-icon-line","layui-icon-minus","layui-icon-question","layui-icon-plus","layui-icon-rollback","layui-icon-small-dash","layui-icon-pause","layui-icon-bg-colors","layui-icon-crown","layui-icon-drag","layui-icon-desktop","layui-icon-gift","layui-icon-stop","layui-icon-fire","layui-icon-thunderbolt","layui-icon-check-circle-fill","layui-icon-left-circle-fill","layui-icon-down-circle-fill","layui-icon-minus-circle-fill","layui-icon-close-circle-fill","layui-icon-info-circle-fill","layui-icon-up-circle-fill","layui-icon-right-circle-fill","layui-icon-plus-circle-fill","layui-icon-question-circle-fill","layui-icon-EURO-circle-fill","layui-icon-frown-fill","layui-icon-copyright-circle-fil","layui-icon-CI-circle-fill","layui-icon-compass-fill","layui-icon-Dollar-circle-fill","layui-icon-poweroff-circle-fill","layui-icon-meh-fill","layui-icon-play-circle-fill","layui-icon-Pound-circle-fill","layui-icon-smile-fill","layui-icon-stop-fill","layui-icon-warning-circle-fill","layui-icon-time-circle-fill","layui-icon-trademark-circle-fil","layui-icon-YUAN-circle-fill","layui-icon-heart-fill","layui-icon-piechart-circle-fil","layui-icon-dashboard-fill","layui-icon-message-fill","layui-icon-check-square-fill","layui-icon-down-square-fill","layui-icon-minus-square-fill","layui-icon-close-square-fill","layui-icon-codelibrary-fill","layui-icon-left-square-fill","layui-icon-play-square-fill","layui-icon-up-square-fill","layui-icon-right-square-fill","layui-icon-plus-square-fill","layui-icon-accountbook-fill","layui-icon-carryout-fill","layui-icon-calendar-fill","layui-icon-calculator-fill","layui-icon-interation-fill","layui-icon-project-fill","layui-icon-detail-fill","layui-icon-save-fill","layui-icon-wallet-fill","layui-icon-control-fill","layui-icon-layout-fill","layui-icon-appstore-fill","layui-icon-mobile-fill","layui-icon-tablet-fill","layui-icon-book-fill","layui-icon-redenvelope-fill","layui-icon-safetycertificate-f","layui-icon-propertysafety-fill","layui-icon-insurance-fill","layui-icon-securityscan-fill","layui-icon-file-exclamation-fil","layui-icon-file-add-fill","layui-icon-file-fill","layui-icon-file-excel-fill","layui-icon-file-markdown-fill","layui-icon-file-text-fill","layui-icon-file-ppt-fill","layui-icon-file-unknown-fill","layui-icon-file-word-fill","layui-icon-file-zip-fill","layui-icon-file-pdf-fill","layui-icon-file-image-fill","layui-icon-diff-fill","layui-icon-file-copy-fill","layui-icon-snippets-fill","layui-icon-batchfolding-fill","layui-icon-reconciliation-fill","layui-icon-folder-add-fill","layui-icon-folder-fill","layui-icon-folder-open-fill","layui-icon-database-fill","layui-icon-container-fill","layui-icon-sever-fill","layui-icon-calendar-check-fill","layui-icon-image-fill","layui-icon-idcard-fill","layui-icon-creditcard-fill","layui-icon-fund-fill","layui-icon-read-fill","layui-icon-contacts-fill","layui-icon-delete-fill","layui-icon-notification-fill","layui-icon-flag-fill","layui-icon-moneycollect-fill","layui-icon-medicinebox-fill","layui-icon-rest-fill","layui-icon-shopping-fill","layui-icon-skin-fill","layui-icon-video-fill","layui-icon-sound-fill","layui-icon-bulb-fill","layui-icon-bell-fill","layui-icon-filter-fill","layui-icon-fire-fill","layui-icon-funnelplot-fill","layui-icon-gift-fill","layui-icon-hourglass-fill","layui-icon-home-fill","layui-icon-trophy-fill","layui-icon-location-fill","layui-icon-cloud-fill","layui-icon-customerservice-fill","layui-icon-experiment-fill","layui-icon-eye-fill","layui-icon-like-fill","layui-icon-lock-fill","layui-icon-unlike-fill","layui-icon-star-fill","layui-icon-unlock-fill","layui-icon-alert-fill","layui-icon-api-fill","layui-icon-highlight-fill","layui-icon-phone-fill","layui-icon-edit-fill","layui-icon-pushpin-fill","layui-icon-rocket-fill","layui-icon-thunderbolt-fill","layui-icon-tag-fill","layui-icon-wrench-fill","layui-icon-tags-fill","layui-icon-bank-fill","layui-icon-camera-fill","layui-icon-error-fill","layui-icon-crown-fill","layui-icon-mail-fill","layui-icon-car-fill","layui-icon-printer-fill","layui-icon-shop-fill","layui-icon-setting-fill","layui-icon-USB-fill","layui-icon-golden-fill","layui-icon-build-fill","layui-icon-boxplot-fill","layui-icon-sliders-fill","layui-icon-alibaba","layui-icon-alibabacloud","layui-icon-antdesign","layui-icon-ant-cloud","layui-icon-behance","layui-icon-googleplus","layui-icon-medium","layui-icon-google","layui-icon-IE","layui-icon-amazon","layui-icon-slack","layui-icon-alipay","layui-icon-taobao","layui-icon-zhihu","layui-icon-HTML","layui-icon-linkedin","layui-icon-yahoo","layui-icon-facebook","layui-icon-skype","layui-icon-CodeSandbox","layui-icon-chrome","layui-icon-codepen","layui-icon-aliwangwang","layui-icon-apple","layui-icon-android","layui-icon-sketch","layui-icon-Gitlab","layui-icon-dribbble","layui-icon-instagram","layui-icon-reddit","layui-icon-windows","layui-icon-yuque","layui-icon-Youtube","layui-icon-Gitlab-fill","layui-icon-dropbox","layui-icon-dingtalk","layui-icon-android-fill","layui-icon-apple-fill","layui-icon-HTML-fill","layui-icon-windows-fill","layui-icon-QQ","layui-icon-twitter","layui-icon-skype-fill","layui-icon-weibo","layui-icon-yuque-fill","layui-icon-Youtube-fill","layui-icon-yahoo-fill","layui-icon-wechat-fill","layui-icon-chrome-fill","layui-icon-alipay-circle-fill","layui-icon-aliwangwang-fill","layui-icon-behance-circle-fill","layui-icon-amazon-circle-fill","layui-icon-codepen-circle-fill","layui-icon-CodeSandbox-circle-f","layui-icon-dropbox-circle-fill","layui-icon-github-fill","layui-icon-dribbble-circle-fill","layui-icon-googleplus-circle-f","layui-icon-medium-circle-fill","layui-icon-QQ-circle-fill","layui-icon-IE-circle-fill","layui-icon-google-circle-fill","layui-icon-dingtalk-circle-fill","layui-icon-sketch-circle-fill","layui-icon-slack-circle-fill","layui-icon-twitter-circle-fill","layui-icon-taobao-circle-fill","layui-icon-weibo-circle-fill","layui-icon-zhihu-circle-fill","layui-icon-reddit-circle-fill","layui-icon-alipay-square-fill","layui-icon-dingtalk-square-fill","layui-icon-CodeSandbox-square-f","layui-icon-behance-square-fill","layui-icon-amazon-square-fill","layui-icon-codepen-square-fill","layui-icon-dribbble-square-fill","layui-icon-dropbox-square-fill","layui-icon-facebook-fill","layui-icon-googleplus-square-f","layui-icon-google-square-fill","layui-icon-instagram-fill","layui-icon-IE-square-fill","layui-icon-medium-square-fill","layui-icon-linkedin-fill","layui-icon-QQ-square-fill","layui-icon-reddit-square-fill","layui-icon-twitter-square-fill","layui-icon-sketch-square-fill","layui-icon-slack-square-fill","layui-icon-taobao-square-fill","layui-icon-weibo-square-fill","layui-icon-zhihu-square-fill","layui-icon-zoomout","layui-icon-apartment","layui-icon-audio","layui-icon-audio-fill","layui-icon-robot","layui-icon-zoomin"];
                    return arr;
                },
                unicode: function () {
                    return ["&amp;#xe77d;","&amp;#xe77e;","&amp;#xe77f;","&amp;#xe780;","&amp;#xe781;","&amp;#xe782;","&amp;#xe783;","&amp;#xe784;","&amp;#xe785;","&amp;#xe786;","&amp;#xe787;","&amp;#xe788;","&amp;#xe789;","&amp;#xe78a;","&amp;#xe78b;","&amp;#xe78c;","&amp;#xe78d;","&amp;#xe78e;","&amp;#xe78f;","&amp;#xe790;","&amp;#xe791;","&amp;#xe792;","&amp;#xe793;","&amp;#xe794;","&amp;#xe795;","&amp;#xe796;","&amp;#xe797;","&amp;#xe798;","&amp;#xe799;","&amp;#xe79a;","&amp;#xe79b;","&amp;#xe79c;","&amp;#xe79d;","&amp;#xe79e;","&amp;#xe79f;","&amp;#xe7a0;","&amp;#xe7a1;","&amp;#xe7a2;","&amp;#xe7a3;","&amp;#xe7a4;","&amp;#xe7a5;","&amp;#xe7a6;","&amp;#xe7a7;","&amp;#xe7a8;","&amp;#xe7a9;","&amp;#xe7aa;","&amp;#xe7ab;","&amp;#xe7ac;","&amp;#xe7ad;","&amp;#xe7ae;","&amp;#xe7af;","&amp;#xe7b0;","&amp;#xe7b1;","&amp;#xe7b2;","&amp;#xe7b3;","&amp;#xe7b4;","&amp;#xe7b5;","&amp;#xe7b6;","&amp;#xe7b7;","&amp;#xe7b8;","&amp;#xe7b9;","&amp;#xe7ba;","&amp;#xe7bb;","&amp;#xe7bc;","&amp;#xe7bd;","&amp;#xe7be;","&amp;#xe7bf;","&amp;#xe7c0;","&amp;#xe7c1;","&amp;#xe7c2;","&amp;#xe7c3;","&amp;#xe7c4;","&amp;#xe7c5;","&amp;#xe7c6;","&amp;#xe7c7;","&amp;#xe7c8;","&amp;#xe7c9;","&amp;#xe7ca;","&amp;#xe7cb;","&amp;#xe7cc;","&amp;#xe7cd;","&amp;#xe7ce;","&amp;#xe7cf;","&amp;#xe7d0;","&amp;#xe7d1;","&amp;#xe7d2;","&amp;#xe7d3;","&amp;#xe7d4;","&amp;#xe7d5;","&amp;#xe7d6;","&amp;#xe7d7;","&amp;#xe7d8;","&amp;#xe7d9;","&amp;#xe7da;","&amp;#xe7db;","&amp;#xe7dc;","&amp;#xe7dd;","&amp;#xe7de;","&amp;#xe7df;","&amp;#xe7e0;","&amp;#xe7e1;","&amp;#xe7e2;","&amp;#xe7e3;","&amp;#xe7e4;","&amp;#xe7e5;","&amp;#xe7e6;","&amp;#xe7e7;","&amp;#xe7e8;","&amp;#xe7e9;","&amp;#xe7ea;","&amp;#xe7eb;","&amp;#xe7ec;","&amp;#xe7ed;","&amp;#xe7ee;","&amp;#xe7ef;","&amp;#xe7f0;","&amp;#xe7f1;","&amp;#xe7f2;","&amp;#xe7f3;","&amp;#xe7f4;","&amp;#xe7f5;","&amp;#xe7f6;","&amp;#xe7f7;","&amp;#xe7f8;","&amp;#xe7f9;","&amp;#xe7fa;","&amp;#xe7fb;","&amp;#xe7fc;","&amp;#xe7fd;","&amp;#xe7fe;","&amp;#xe7ff;","&amp;#xe800;","&amp;#xe801;","&amp;#xe802;","&amp;#xe803;","&amp;#xe804;","&amp;#xe805;","&amp;#xe806;","&amp;#xe807;","&amp;#xe808;","&amp;#xe809;","&amp;#xe80a;","&amp;#xe80b;","&amp;#xe80c;","&amp;#xe80d;","&amp;#xe80e;","&amp;#xe80f;","&amp;#xe810;","&amp;#xe811;","&amp;#xe812;","&amp;#xe813;","&amp;#xe814;","&amp;#xe815;","&amp;#xe816;","&amp;#xe817;","&amp;#xe818;","&amp;#xe819;","&amp;#xe81a;","&amp;#xe81b;","&amp;#xe81c;","&amp;#xe81d;","&amp;#xe81e;","&amp;#xe81f;","&amp;#xe820;","&amp;#xe821;","&amp;#xe822;","&amp;#xe823;","&amp;#xe824;","&amp;#xe825;","&amp;#xe826;","&amp;#xe827;","&amp;#xe828;","&amp;#xe829;","&amp;#xe82a;","&amp;#xe82b;","&amp;#xe82c;","&amp;#xe82d;","&amp;#xe82e;","&amp;#xe82f;","&amp;#xe830;","&amp;#xe831;","&amp;#xe832;","&amp;#xe833;","&amp;#xe834;","&amp;#xe835;","&amp;#xe836;","&amp;#xe837;","&amp;#xe838;","&amp;#xe839;","&amp;#xe83a;","&amp;#xe83b;","&amp;#xe83c;","&amp;#xe83d;","&amp;#xe83e;","&amp;#xe83f;","&amp;#xe840;","&amp;#xe841;","&amp;#xe842;","&amp;#xe843;","&amp;#xe844;","&amp;#xe845;","&amp;#xe846;","&amp;#xe847;","&amp;#xe848;","&amp;#xe849;","&amp;#xe84a;","&amp;#xe84b;","&amp;#xe84c;","&amp;#xe84d;","&amp;#xe84e;","&amp;#xe84f;","&amp;#xe850;","&amp;#xe851;","&amp;#xe852;","&amp;#xe853;","&amp;#xe854;","&amp;#xe855;","&amp;#xe856;","&amp;#xe857;","&amp;#xe858;","&amp;#xe859;","&amp;#xe85a;","&amp;#xe85b;","&amp;#xe85c;","&amp;#xe85d;","&amp;#xe85e;","&amp;#xe85f;","&amp;#xe860;","&amp;#xe861;","&amp;#xe862;","&amp;#xe863;","&amp;#xe864;","&amp;#xe865;","&amp;#xe866;","&amp;#xe867;","&amp;#xe868;","&amp;#xe869;","&amp;#xe86a;","&amp;#xe86b;","&amp;#xe86c;","&amp;#xe86d;","&amp;#xe86e;","&amp;#xe86f;","&amp;#xe870;","&amp;#xe871;","&amp;#xe872;","&amp;#xe873;","&amp;#xe874;","&amp;#xe875;","&amp;#xe876;","&amp;#xe877;","&amp;#xe878;","&amp;#xe879;","&amp;#xe87a;","&amp;#xe87b;","&amp;#xe87c;","&amp;#xe87d;","&amp;#xe87e;","&amp;#xe87f;","&amp;#xe880;","&amp;#xe881;","&amp;#xe882;","&amp;#xe883;","&amp;#xe884;","&amp;#xe885;","&amp;#xe886;","&amp;#xe887;","&amp;#xe888;","&amp;#xe889;","&amp;#xe88a;","&amp;#xe88b;","&amp;#xe88c;","&amp;#xe88d;","&amp;#xe88e;","&amp;#xe88f;","&amp;#xe890;","&amp;#xe891;","&amp;#xe892;","&amp;#xe893;","&amp;#xe894;","&amp;#xe895;","&amp;#xe896;","&amp;#xe897;","&amp;#xe898;","&amp;#xe899;","&amp;#xe89a;","&amp;#xe89b;","&amp;#xe89c;","&amp;#xe89d;","&amp;#xe89e;","&amp;#xe89f;","&amp;#xe8a0;","&amp;#xe8a1;","&amp;#xe8a2;","&amp;#xe8a3;","&amp;#xe8a4;","&amp;#xe8a5;","&amp;#xe8a6;","&amp;#xe8a7;","&amp;#xe8a8;","&amp;#xe8a9;","&amp;#xe8aa;","&amp;#xe8ab;","&amp;#xe8ac;","&amp;#xe8ad;","&amp;#xe8ae;","&amp;#xe8af;","&amp;#xe8b0;","&amp;#xe8b1;","&amp;#xe8b2;","&amp;#xe8b3;","&amp;#xe8b4;","&amp;#xe8b5;","&amp;#xe8b6;","&amp;#xe8b7;","&amp;#xe8b8;","&amp;#xe8b9;","&amp;#xe8ba;","&amp;#xe8bb;","&amp;#xe8bc;","&amp;#xe8bd;","&amp;#xe8be;","&amp;#xe8bf;","&amp;#xe8c0;","&amp;#xe8c1;","&amp;#xe8c2;","&amp;#xe8c3;","&amp;#xe8c4;","&amp;#xe8c5;","&amp;#xe8c6;","&amp;#xe8c7;","&amp;#xe8c8;","&amp;#xe8c9;","&amp;#xe8ca;","&amp;#xe8cb;","&amp;#xe8cc;","&amp;#xe8cd;","&amp;#xe8ce;","&amp;#xe8cf;","&amp;#xe8d0;","&amp;#xe8d1;","&amp;#xe8d2;","&amp;#xe8d3;","&amp;#xe8d4;","&amp;#xe8d5;","&amp;#xe8d6;","&amp;#xe8d7;","&amp;#xe8d8;","&amp;#xe8d9;","&amp;#xe8da;","&amp;#xe8db;","&amp;#xe8dc;","&amp;#xe8dd;","&amp;#xe8de;","&amp;#xe8df;","&amp;#xe8e0;","&amp;#xe8e1;","&amp;#xe8e2;","&amp;#xe8e3;","&amp;#xe8e4;","&amp;#xe8e5;","&amp;#xe8e6;","&amp;#xe8e7;","&amp;#xe8e8;","&amp;#xe8e9;","&amp;#xe8ea;","&amp;#xe8eb;","&amp;#xe8ec;","&amp;#xe8ed;","&amp;#xe8ee;","&amp;#xe8ef;","&amp;#xe8f0;","&amp;#xe8f1;","&amp;#xe8f2;","&amp;#xe8f3;","&amp;#xe8f4;","&amp;#xe8f5;","&amp;#xe8f6;","&amp;#xe8f7;","&amp;#xe8f8;","&amp;#xe8f9;","&amp;#xe8fa;","&amp;#xe8fb;","&amp;#xe8fc;","&amp;#xe8fd;","&amp;#xe8fe;","&amp;#xe8ff;","&amp;#xe900;","&amp;#xe901;","&amp;#xe902;","&amp;#xe903;","&amp;#xe904;","&amp;#xe905;","&amp;#xe906;","&amp;#xe907;","&amp;#xe908;","&amp;#xe909;","&amp;#xe90a;","&amp;#xe90b;","&amp;#xe90c;","&amp;#xe90d;","&amp;#xe90e;","&amp;#xe90f;","&amp;#xe910;","&amp;#xe911;","&amp;#xe912;","&amp;#xe913;","&amp;#xe914;","&amp;#xe915;","&amp;#xe916;","&amp;#xe917;","&amp;#xe918;","&amp;#xe919;","&amp;#xe91a;","&amp;#xe91b;","&amp;#xe91c;","&amp;#xe91d;","&amp;#xe91e;","&amp;#xe91f;","&amp;#xe920;","&amp;#xe921;","&amp;#xe922;","&amp;#xe923;","&amp;#xe924;","&amp;#xe925;","&amp;#xe926;","&amp;#xe927;","&amp;#xe928;","&amp;#xe929;","&amp;#xe92a;","&amp;#xe92b;","&amp;#xe92c;","&amp;#xe92d;","&amp;#xe92e;","&amp;#xe92f;","&amp;#xe930;","&amp;#xe931;","&amp;#xe932;","&amp;#xe933;","&amp;#xe934;","&amp;#xe935;","&amp;#xe936;","&amp;#xe937;","&amp;#xe938;","&amp;#xe939;","&amp;#xe93a;","&amp;#xe93b;","&amp;#xe93c;","&amp;#xe93d;","&amp;#xe93e;","&amp;#xe93f;","&amp;#xe940;","&amp;#xe941;","&amp;#xe942;","&amp;#xe943;","&amp;#xe944;","&amp;#xe945;","&amp;#xe946;","&amp;#xe947;","&amp;#xe948;","&amp;#xe949;","&amp;#xe94a;","&amp;#xe94b;","&amp;#xe94c;","&amp;#xe94d;","&amp;#xe94e;","&amp;#xe94f;","&amp;#xe950;","&amp;#xe951;","&amp;#xe952;","&amp;#xe953;","&amp;#xe954;","&amp;#xe955;","&amp;#xe956;","&amp;#xe957;","&amp;#xe958;","&amp;#xe959;","&amp;#xe95a;","&amp;#xe95b;","&amp;#xe95c;","&amp;#xe95d;","&amp;#xe95e;","&amp;#xe95f;","&amp;#xe960;","&amp;#xe961;","&amp;#xe962;","&amp;#xe963;","&amp;#xe964;","&amp;#xe965;","&amp;#xe966;","&amp;#xe967;","&amp;#xe968;","&amp;#xe969;","&amp;#xe96a;","&amp;#xe96b;","&amp;#xe96c;","&amp;#xe96d;","&amp;#xe96e;","&amp;#xe96f;","&amp;#xe970;","&amp;#xe971;","&amp;#xe972;","&amp;#xe973;","&amp;#xe974;","&amp;#xe975;","&amp;#xe976;","&amp;#xe977;","&amp;#xe978;","&amp;#xe979;","&amp;#xe97a;","&amp;#xe97b;","&amp;#xe97c;","&amp;#xe97d;","&amp;#xe97e;","&amp;#xe97f;","&amp;#xe980;","&amp;#xe981;","&amp;#xe982;","&amp;#xe983;","&amp;#xe984;","&amp;#xe985;","&amp;#xe986;","&amp;#xe987;","&amp;#xe988;","&amp;#xe989;","&amp;#xe98a;","&amp;#xe98b;","&amp;#xe98c;","&amp;#xe98d;","&amp;#xe98e;","&amp;#xe98f;","&amp;#xe990;"];
                }
            }
        };

        a.init();
        return new IconPicker();
    };

    /**
     * 选中图标
     * @param filter lay-filter
     * @param iconName 图标名称，自动识别fontClass/unicode
     */
    IconPicker.prototype.checkIcon = function (filter, iconName){
        var p = $('*[lay-filter='+ filter +']').next().find('.layui-iconpicker-item .layui-icon'),
            c = iconName;

        if (c.indexOf('#xe') > 0){
            p.html(c);
        } else {
            p.html('').attr('class', 'layui-icon ' + c);
        }
    };

    var iconPicker = new IconPicker();
    exports(_MOD, iconPicker);
});

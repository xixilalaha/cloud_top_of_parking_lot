{
	"code": 0,
	"msg": "ok",
        "data": [
       
            {
    "title":"系统设置",
    "icon": "layui-icon-setting-fill",
    "childs":[{
        "title": "用户管理",
        "href":"/system/user"
    },{
        "title": "角色管理",
        "href":"/system/roles"
    },{
        "title": "权限管理",
        "href":"/system/permissionNew"
    },{
        "title": "菜单管理",
        "href":"/system/menu"
    }]
},{
    "title":"月卡会员",
    "icon": "layui-icon-unorderedlist",
    "childs":[{
        "title": "月卡办理记录",
        "href":"/monthMember/monthRegisteList"
    },{
        "title": "月卡缴纳记录",
        "href":"/monthMember/monthPayList"
    }]
},{
    "title":"会员CRM",
    "icon": "layui-icon-unorderedlist",
    "childs":[{
        "title": "会员信息",
        "href":"/memberCRM/memberInfo"
    }]
},{
    "title":"车场设置",
    "icon":"layui-icon-car",
    "childs":[{
        "title":"车场设置",
        "href":"/parkingLot/parkingSet"
    },{
        "title": "岗亭管理",
        "href":"/parkingLot/policeBox"
    },{
        "title": "收费规则",
        "href":"/parkingLot/moneyRules"
    }]
},{
    "title":"商家管理",
    "icon":"layui-icon-dashboard-fill",
    "childs":[{
        "title":"商家管理",
        "href":"/seller/seller"
    },{
        "title":"优惠券管理",
        "href":"/seller/couponManage"
    },{
        "title":"购券记录",
        "href":"/seller/couponRecord"
    }]
},{
    "title":"车辆管理",
    "icon":"layui-icon-car",
    "childs":[{
        "title":"车辆信息",
        "href":"/parkingCar/parkingCarInfo"
    },{
        "title": "车辆认证",
        "href":"/parkingCar/carApply"
    },{
        "title": "出入记录",
        "href":"/parkingCar/InOutList"
    }]
},{
    "title":"运营分析",
    "icon":"layui-icon-project",
    "childs":[{
        "title": "车场收入",
        "href":"/operateAnalysis/parkIncome"
    },{
        "title": "收费统计",
        "href":"/operateAnalysis/countList"
    },{
        "title": "收费员日报",
        "href":"/operateAnalysis/userCountList"
    },{
        "title":"支付流水",
        "href":"/operateAnalysis/payList"
    }]
}
]
}
